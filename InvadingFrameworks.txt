Reducing patterns tend to be simple... and safe. But... 
reducing also entails helping the opponent to solidify his territory. 

The subtle workings of reducing moves cannot be learnt from a book but have
to be absorbed from actual experience on the go board.
The positional judgment on which reducing moves should be based involves
not just a comparison of territories but also an overall assesment of the
relative weakness and strength of all the groups pn the board. 


FUSEKI:______________________________________________
  Defensive    || Invading
1)       Empty corners
2) Enclosure   || Approach      [hoshi are an exception]
3)      Extensions              [often third line stones]
4)      Checking Extensions     [extensions checking opponent's extensions]
5) Jumps       || Reductions
______________________________________________________

            Reduction           Invasion
Territory   Limits              Destroies
Locality    Global Board        Emphasizes Local Area
Riskyness   Quite safe          Risky
Competences Positional judgment Tesuji knowledge

[Depending on the whole board the same pattern can have different implications]

Twelve secondary objectives.
 1) Maintaning territorial balance
 2) Limiting a Moyo 
 3) Probing the opponent's rensponse 
 4) Expanding one's own Moyo
 5) Creating a foothold for an invasion 
 6) Mutual reduction 
 7) Building centre territory 
 8) Taking aim at defects in the opponent's shape 
 9) Maintaining the balance of the influence 
10) Reinfprce weak stones 
11) Making preparations for an attack 
12) Following a shinogi strategy

__________________
WHERE (local)
__________________
The aim of reducing is to prevent the opponent from forming a moyo,
this is why reductions are often directed towards third line stones.

All the 9 points in the square on the top of the 3rd line stone are
possible points for a reduction: shoulder hit, contact move, capping,
knight, large knight, 2points.

A 4th line stone should be invaded, not reduced.
Reduction points are 5th line knight, side contsct, diagonal 5th line
and capping. 

Corner enclosure followd the same principles (if you have to reduce,
treat the 4th line stone as if it were a 3rd line one). Remark, that
often an invasion is better than a reduction in this cases.

When an opponent has a deep moyo it happens that it's impossible to play
inside. In that case one the best position to play is on the sector line. 
__________________
WHERE (global)
__________________
1) What is the size of the moyo my opponent gets if I don't reduce?
2) What is the value of the follow up it creates?
3) In evaluating 2, keep into account of the strength, weaknesses
   that both players have around.

__________________
HOW (preparation)
__________________
The checking extensions phase is also a preparation for the next one.
I have to prepare my reduction moves, I do it in this phase where I have
to keep into account of reducing moves.

Other preparatory moves could be peeps (to make overconcentrated the
opponent), probes, ...

All this to maximize the efficency of the reduction.


____________________________
Invading on the sector line.
____________________________
There are no josekis. You invade on the sector line when playing deeper
would be capped and eyeless. If you invade too high the opponent will
secure a lot of territory, if you play too low he will capture you.
Invading on the sector line can be a mistake and when choosing where to
one has to consider the defects the opponent's moyo has. Figure out the
board with two more moves and wonder if the defects have changed, are weaker,
or have disappeared.
